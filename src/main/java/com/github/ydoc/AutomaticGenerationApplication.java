package com.github.ydoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * author NoBugBoY
 */
@SpringBootApplication
public class AutomaticGenerationApplication {

    public static void main(String[] args) {
	SpringApplication.run(AutomaticGenerationApplication.class, args);
    }

}
