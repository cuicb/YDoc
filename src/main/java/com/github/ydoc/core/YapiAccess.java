package com.github.ydoc.core;

import lombok.Data;

/**
 * author NoBugBoY description create 2021-06-24 16:21
 **/
@Data
public class YapiAccess {
    private String token;
    private Integer uid;
}
